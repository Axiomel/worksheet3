%FEM computations with calfem
close all;
clear all;
data;

K = zeros(ndof);
a = zeros(ndof,1);
D = hooke(1,E,v);
[ex,ey]=coordxtr(edof,coord,dof,3);

for i=1:nelm
    Ke = plante(ex(i,:),ey(i,:),ep,D);
%   K = assem(edof(i,:),K,Ke);
    %Alternatively:
    indx = edof(i,2:end);
    K(indx,indx) = K(indx,indx) + Ke;
end

[a,r] = solve(K,loads,bcs);
disp('FEM analysis utilising CALFEM')
disp('---------------------')
disp('Displacements:')
disp(a);
disp('---------------------')
disp('Reaction force')
disp(r)

figure()
eldraw2(ex,ey,[1 2 1]);
hold on;
ed = extract(edof,a);
eldisp2(ex,ey,ed,[1 4 1],30)