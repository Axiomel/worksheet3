# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 14:05:25 2018

@author: Henningsson, Fredriksson
"""

import numpy as np
import calfem.core as cfc
import json

class InputData(object):
    
    def __init__(self):
        """Klass för att definiera indata för vår modell."""
        self.version = 1
        self.t = 0.01
        self.v=0.3
        self.ep = [1, self.t]
        # --- Elementegenskaper
        self.E = 208*(10**9)
        # --- Skapa indata för testexempel
        self.coord = np.array([
			   [0.0, 0.0],
			   [0.0, 0.1],
			   [0.1, 0.0],
			   [0.1, 0.1],
			   [0.2, 0.0],
			   [0.2, 0.1]])
        # --- Elementtopolgi
        self.edof = np.array([
            [1, 2, 7, 8, 3, 4],
            [1, 2, 5, 6, 7, 8],
            [5, 6, 11, 12, 7, 8],
				[5, 6, 9, 10, 11, 12]])
        self.dof = np.array([
			   [1, 2],
			   [3, 4],
			   [5, 6],
			   [7, 8],
			   [9, 10],
			   [11, 12]])
        self.ndof = np.size(self.dof)
        # --- Laster
        self.loads = np.array([
            [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
				 [0.0],
			 [-10000.0]
        ])
        # --- Randvillkor
        self.bcs = np.array([1,2,3,4])
        
    def save(self, filename):
        """Spara indata till fil."""

        inputData = {}
        inputData["version"] = self.version
        inputData["t"] = self.t
        inputData["ep"] = self.ep
        inputData["v"] = self.v
        inputData["E"] = self.E
        inputData["coord"] = self.coord.tolist()
        inputData["edof"] = self.edof.tolist()
        inputData["dof"] = self.dof.tolist()
        inputData["ndof"] = self.ndof
        inputData["loads"] = self.loads.tolist()
        inputData["bcs"] = self.bcs.tolist()

        ofile = open(filename, "w")
        json.dump(inputData, ofile, sort_keys = True, indent = 4)
        ofile.close()

    def load(self, filename):
        """Läs indata från fil."""

        ifile = open(filename, "r")
        inputData = json.load(ifile)
        ifile.close()

        self.version = inputData["version"]
        self.t = inputData["t"]
        self.ep = inputData["ep"]
        self.v = inputData["v"] 
        self.E = inputData["E"] 
        self.coord = np.asarray(inputData["coord"])
        self.edof = np.asarray(inputData["edof"]) 
        self.dof = np.asarray(inputData["dof"]) 
        self.ndof = inputData["ndof"]
        self.loads = np.asarray(inputData["loads"])
        self.bcs = np.asarray(inputData["bcs"]) 
               
class Solver(object):

    """Klass för att hantera lösningen av vår beräkningsmodell."""
    def __init__(self, inputData, outputData):
        self.inputData = inputData
        self.outputData = outputData

    def execute(self):

        # --- Överför modell variabler till lokala referenser
        E = self.inputData.E
        v = self.inputData.v
        edof = self.inputData.edof
        coord = self.inputData.coord
        dof = self.inputData.dof
        ndof = self.inputData.ndof
        ep = self.inputData.ep		    
        loads = self.inputData.loads
        bcs = self.inputData.bcs
        # --- Beräkningskod
        K = np.zeros((ndof,ndof))
        D = cfc.hooke(1,E,v)
        nelm = np.shape(edof)[0]
        Ex , Ey = cfc.coordxtr(edof, coord, dof)
			
        for i in range(0,nelm):
            Ke = cfc.plante(Ex[i,:],Ey[i,:],ep,D)
            cfc.assem(edof[i,:],K,Ke)
            
        a, r = cfc.solveq(K,loads,bcs)    
            
			# --- Överför modell variabler till lokala referenser

        self.outputData.displacements = a
        self.outputData.reactionForces = r

class OutputData(object):
	
	def __init__(self):
		self.displacements = None
		self.reactionForces = None
        
class Report(object):
    """Klass för presentation av indata och utdata i rapportform."""
    def __init__(self, inputData, outputData):
        self.inputData = inputData
        self.outputData = outputData
        self.report = ""

    def clear(self):
        self.report = ""

    def addText(self, text=""):
        self.report+=str(text)+"\n"

    def __str__(self):
        self.clear()
        self.addText()
        self.addText("-------------- Model input ----------------------------------")
        self.addText()
        self.addText("P L A N E  S T R E S S")
        self.addText()
        self.addText("Thickness:")
        self.addText()
        self.addText(self.inputData.t)
        self.addText()
        self.addText("Young's Modulus:")
        self.addText()
        self.addText(self.inputData.E)
        self.addText()
        self.addText("Poission's Ratio:")
        self.addText()
        self.addText(self.inputData.v)
        self.addText()
        self.addText("Coordinates:")
        self.addText()
        self.addText(self.inputData.coord)
        self.addText()
        self.addText("Edof:")
        self.addText()
        self.addText(self.inputData.edof)
        self.addText()
        self.addText("Dof:")
        self.addText()
        self.addText(self.inputData.dof)
        self.addText()
        self.addText("Loads:")
        self.addText()
        self.addText(self.inputData.loads)
        self.addText()
        self.addText("Boundary Conditions:")
        self.addText()
        self.addText(self.inputData.bcs)
        self.addText()
        self.addText("-------------- RESULTS ----------------------------------")
        self.addText()
        self.addText("Displacements:")
        self.addText()
        self.addText(self.outputData.displacements)
        self.addText()
        self.addText("Reaction Forces:")
        self.addText()
        self.addText(self.outputData.reactionForces)
        self.addText()
        return self.report
