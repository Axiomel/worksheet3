%Data file, with topology etc.

t = 0.01;
E = 208*(10^9);
v = 0.3;
ep = [1 t];

coord = [0 0;
        0 0.1;
        0.1 0;
        0.1 0.1;
        0.2 0;
        0.2 0.1];

edof = [1 1 2 7 8 3 4;
        2 1 2 5 6 7 8;
        3 5 6 11 12 7 8;
        4 5 6 9 10 11 12];

    
nelm = length(edof(:,1));

dof = [1 2;
       3 4;
       5 6;
       7 8;
       9 10;
       11 12];
   
 ndof = max(max(dof)); 
 
 loads = zeros(ndof,1);
 loads(end,1)=-10000;
 
 bcs = [1 0;
        2 0;
        3 0;
        4 0;]; 
    